//
//  SmartCamera.swift
//   2020
//

import UIKit
import AVFoundation

public class SmartCamera: NSObject, CameraControlsProviding, AVCaptureMetadataOutputObjectsDelegate {
    
    private var metaDataObjectTypes: [AVMetadataObject.ObjectType] = []

    public var detectionCapabilities: [CameraDetectionCapabilities] = [] {
        didSet {
            detectionCapabilities.forEach { (capability) in
                switch capability {
                case .barcode:
                    metaDataObjectTypes.append(contentsOf: [.ean8, .ean13, .pdf417])
                case .qr:
                    metaDataObjectTypes.append(.qr)
                }
            }
        }
    }
    
    weak public var delegate: CameraConstrolsProvidingListener?
    
    private var captureSession: AVCaptureSession?
    private var previewLayer: AVCaptureVideoPreviewLayer?
    
    public var view: ThemedView

    public func start() {

        if let captureSession = captureSession, !captureSession.isRunning {
            captureSession.startRunning()
            return
        }

        initializeCamera()
        
        guard let captureSession = captureSession, !captureSession.isRunning else { return }
        
        addPreviewLayer()
        captureSession.startRunning()
    }
    
    public func stop() {
        guard let captureSession = captureSession, captureSession.isRunning else { return }
        captureSession.stopRunning()
        self.captureSession = nil
        removePreviewLayer()
    }
    
    public var isRunning: Bool {
        guard let captureSession = captureSession else { return false }
        return captureSession.isRunning
    }

    public init(themeStream: ThemeStreaming) {
        self.view = ThemedView(themeStream: themeStream)
        super.init()
        view.clipsToBounds = true
    }
    
    private func initializeCamera() {
        self.captureSession = AVCaptureSession()
        guard let captureSession = captureSession else { return }
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }

        let videoInput: AVCaptureDeviceInput

        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }

        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            delegate?.barCodeScanningFailed(error: "Can't add input")
            return
        }

        let metadataOutput = AVCaptureMetadataOutput()

        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.ean8, .ean13, .pdf417, .qr]
        } else {
            delegate?.barCodeScanningFailed(error: "Camera initialization failed")
            return
        }
    }

    private func removePreviewLayer() {
        guard let previewLayer = previewLayer else { return }
        previewLayer.removeFromSuperlayer()
        self.previewLayer = nil
        view.isHidden = true
    }
    
    private func addPreviewLayer() {
        guard let captureSession = captureSession else { return }
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        self.previewLayer = previewLayer
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        view.isHidden = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {

        guard let captureSession = captureSession else { return }
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            delegate?.barCodeScanned(code: stringValue)
            captureSession.stopRunning()
        }
    }

}

public protocol CameraControlsProviding : class {
    func start()
    func stop()
    var isRunning: Bool { get }
    var view: ThemedView { get }
    var delegate: CameraConstrolsProvidingListener? { get set }
    var detectionCapabilities: [CameraDetectionCapabilities] { get set }
}

public protocol CameraConstrolsProvidingListener : class {
    func barCodeScanned(code: String)
    func barCodeScanningFailed(error: String)
}

public enum CameraDetectionCapabilities {
    case qr, barcode
}
