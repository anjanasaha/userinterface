//
//  NavigationController.swift
//   2020
//

import UIKit
import RxSwift

open class NavigationController: UINavigationController {

    var disposeBag: DisposeBag? = DisposeBag()
    var themeStream: ThemeStreaming?

    public init(rootViewController: UIViewController, themeStream: ThemeStreaming) {
        self.themeStream = themeStream
        super.init(rootViewController: rootViewController)

        applyTheming()
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        
        applyTheming()
    }

    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    public convenience init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, themeStream: ThemeStreaming) {
        self.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.themeStream = themeStream
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func applyTheming() {
        navigationBar.prefersLargeTitles = true
        navigationItem.largeTitleDisplayMode = .always

        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
        view.backgroundColor = .clear
    }

    deinit {
        disposeBag = nil
    }
}

