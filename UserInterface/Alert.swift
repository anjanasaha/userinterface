//
//  Alert.swift
//   2020
//

import UIKit

public protocol AlertViewModelling {
    var header: String { get }
    var title: String { get set }
    var subtitle: String { get set }
    var icon: UIImage? { get set }
    var alertViewType: AlertViewType { get set }
}

public enum AlertViewType {
    case `default`, success, error
}

public struct AlertViewModel: AlertViewModelling {

    // TODO: Localization
    public var header: String {
        switch alertViewType {
        case .success:
            return "Success"
        case .error:
            return "Error"
        default:
            return ""
        }
    }
    
    public var alertViewType: AlertViewType = .default
    
    public var title: String
    
    public var subtitle: String
    
    public var icon: UIImage?

    public init(alertViewType: AlertViewType = .default, title: String, subtitle: String, icon: UIImage?  = nil) {
        self.alertViewType = alertViewType
        self.title = title
        self.subtitle = subtitle
        self.icon = icon
    }
}

public class Alert {
    
    private var viewModel: AlertViewModelling

    public init(viewModel: AlertViewModelling) {
        self.viewModel = viewModel
    }

    public func show(in viewController: ViewController) {

        let alert = UIAlertController(title: viewModel.title,
                                      message: viewModel.subtitle,
                                      preferredStyle: .actionSheet)
        // TODO
        let defaultAction = UIAlertAction(title: "Ok", style: .cancel) { _ in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(defaultAction)
        
        viewController.present(alert, animated: true)
    }
}
