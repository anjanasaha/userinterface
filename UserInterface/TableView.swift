//
//  TableView.swift
//   2020
//

import UIKit

open class TableView: UITableView {
    
    let themeStream: ThemeStreaming

    init(frame: CGRect, style: UITableView.Style, themeStream: ThemeStreaming) {
        self.themeStream = themeStream
        super.init(frame: frame, style: style)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
