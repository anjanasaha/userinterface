//
//  Color.swift
//  Copyright © 2020 HashBytes Technologies LLP. All rights reserved. 2020
//

import UIKit

public final class Color {
    public static let white: UIColor = .white
    public static let red: UIColor = .systemRed
    public static let green: UIColor = .systemGreen
    public static let cyan: UIColor = .cyan
    public static let gray90: UIColor = UIColor(hex: "#4A4A4A")
    public static let black: UIColor = .black
    public static let gray60: UIColor = .systemGray6
    public static let gray40: UIColor = .systemGray4
    public static let gray20: UIColor = .systemGray2
    public static let orange: UIColor = .systemOrange //UIColor(hex: "#FF9800")
    public static let lightBlue: UIColor = UIColor(hex: "#479cff")
    
    public static let blue: UIColor = .systemBlue //UIColor(hex: "#147efb")
    public static let blueDisabled: UIColor = .systemBlue //UIColor(hex: "#147efb")
    public static let blueHighlighted: UIColor = .systemBlue //UIColor(hex: "#147efb")
    
    public static let cyanDisabled: UIColor = .tertiarySystemBackground
    public static let cyanHighlighted: UIColor = .cyan
    
    public static let greenDisabled: UIColor = .secondarySystemBackground
    public static let greenHighlighted: UIColor = .systemGreen
    
    public static let redDisabled: UIColor = .tertiarySystemBackground
    public static let redHighlighted: UIColor = .systemRed
    
    public static let transparent: UIColor = .clear
    public static let link: UIColor = .link //UIColor(hex: "#147efb")
    public static let primaryText: UIColor = .label
    public static let secondaryText: UIColor = .secondaryLabel
    public static let tertiaryText: UIColor = .tertiaryLabel
}
