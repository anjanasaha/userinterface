//
//  KeyboardManager.swift
//   2020
//

import UIKit
import RxSwift

public protocol KeyboardReactable: class {
    func keyboardStatus(event: KeyboardEvent, height: CGFloat)
}

public protocol KeyboardObserving: class {
    var status: Observable<KeyboardEventHeight> { get }
}

public protocol KeyboardManaging: KeyboardObserving {
    func update(event: KeyboardEvent, height: CGFloat)
}

public enum KeyboardEvent {
    case willOpen, opened, willClose, closed
}

public typealias KeyboardEventHeight = (KeyboardEvent, CGFloat)

public final class KeyboardManager: NSObject, KeyboardManaging {
    
    private var statusSubject: PublishSubject<KeyboardEventHeight>

    var keyboardEventHeight: KeyboardEventHeight = (.closed, 0.0)
    
    public var status: Observable<KeyboardEventHeight> {
        return statusSubject.asObservable()
    }
    
    public func update(event: KeyboardEvent, height: CGFloat) {
        keyboardEventHeight = (event, height)
        statusSubject.onNext(keyboardEventHeight)
    }
    
    public override init() {
        self.statusSubject = PublishSubject<KeyboardEventHeight>()
        super.init()
        registerKeyboardNotifications()
    }
    
    deinit {
        deregisterKeyboardNotifications()
    }

    private func registerKeyboardNotifications() {
        let center = NotificationCenter.default
        center.addObserver(self, selector: #selector(keyboardWillBeShown(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        center.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        center.addObserver(self, selector: #selector(keyboardDidHide(notification:)), name: UIResponder.keyboardDidHideNotification, object: nil)
        center.addObserver(self, selector: #selector(keyboardDidShow(notification:)), name: UIResponder.keyboardDidShowNotification, object: nil)
    }

    private func deregisterKeyboardNotifications() {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification , object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc
    func keyboardWillBeShown(notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            update(event: .willOpen, height: keyboardHeight)
        }
    }

    @objc
    func keyboardWillBeHidden(notification: Notification) {
        update(event: .willClose, height: 0.0)
    }

    @objc
    func keyboardDidHide(notification: Notification) {
        update(event: .closed, height: 0.0)
    }

    @objc
    func keyboardDidShow(notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            update(event: .opened, height: keyboardHeight)
        }
    }
}
