//
//  TextField.swift
//  Copyright © 2020 HashBytes Technologies LLP. All rights reserved. 2020
//

import UIKit
import RxSwift

open class TextField: UITextField, Themeable {
    
    public var themeStream: ThemeStreaming
    
    private var currentTheme: Theme?
    
    public var disposeBag = DisposeBag()
    
    public var style: TextStyle {
        didSet {
            updateStyle()
        }
    }

    open override var placeholder: String? {
        didSet {
            updateStyle()
        }
    }

    public init(themeStream: ThemeStreaming, style: TextStyle) {
        self.themeStream = themeStream
        self.style = style
        super.init(frame: .zero)
        
        themeStream.themeInUse.subscribe(onNext: { [weak self] (theme) in
            guard let strongSelf = self else { return }
            strongSelf.currentTheme = theme
            strongSelf.updateStyle()
        }).disposed(by: disposeBag)

    }

    open override func layoutSubviews() {
        super.layoutSubviews()
        updateStyle()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func updateStyle() {
        guard let theme = currentTheme else { return }
        font = theme.font(style)
        attributedPlaceholder = NSAttributedString(string: placeholder ?? "",
                                                   attributes: [NSAttributedString.Key.foregroundColor: theme.placeHolder(self.style) ])
        minimumFontSize = CGFloat((UIDevice().isSmallSize ? Font.FontSize.standard(.small).value : Font.FontSize.standard(.regular).value))
        textColor = .label
        keyboardAppearance = theme.keyboardAppearance
        backgroundColor = .clear
        layer.cornerRadius = 1.dip
        autocorrectionType = .no
    }
    
    public var height: CGFloat {
        switch style {
        case .largeSuccess, .largeInfo, .largeWarning, .largeDanger, .largePrimary, .largeLink, .largePillInfo, .largePillSuccess, .largePillDanger, .largePillPrimary, .largePillWarning:
            return Height.large
        case .smallSecondary, .smallInfo, .smallWarning, .smallDanger, .smallPrimary, .smallSuccess, .smallPillDanger, .smallPillSuccess, .smallPillPrimary, .smallLink, .smallPillInfo, .smallPillWarning, .smallTertiary:
            return Height.small
        case .regularSuccess, .regularInfo, .regularWarning, .regularDanger, .regularPrimary, .regularPrimaryBold, .regularLink, .regularPillInfo, .regularPillSuccess, .regularPillDanger, .regularPillPrimary, .regularPillWarning, .regularSecondary:
            return Height.regular
        case .tinyPillSuccess, .tinyInfo, .tinySuccess, .tinyDanger, .tinyPrimary, .tinyWarning, .tinyLink, .tinyPillInfo, .tinyPillDanger, .tinyPillPrimary, .tinyPillWarning:
            return Height.tiny
        }
    }
    
    private struct Height {
        static let tiny = 3.5.dip
        static let small = 4.0.dip
        static let regular = 5.dip
        static let large = 6.5.dip
    }
}
