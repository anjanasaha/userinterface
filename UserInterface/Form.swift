//
//  Form.swift
//   2020
//

import UIKit
import RIBs

public class Form: NSObject {
    public let sections: [Section]
    public let title: String

    init(title: String, sections: [Section]) {
        self.sections = sections
        self.title = title
        super.init()
    }
}

public protocol FormItemBuildable {
    func build(listener: FormListener?) -> ViewableRouting
}

public protocol FormListener: class {
    func didTapBack(popped: Bool)
}
