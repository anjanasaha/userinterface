//
//  LightTheme.swift
//  Copyright © 2020 HashBytes Technologies LLP. All rights reserved. 2020
//

import UIKit

public class LightTheme: Theme {

    public var optionSelectionColor: UIColor = .systemGray4
    
    public var type: ThemeType = .light

    public var themeStream: ThemeStreaming?
    
    public var fadedBackgroundColor: UIColor {
        return .clear
    }
    
    public func buttonBgColor(_ textStyle: TextStyle, for controlState: UIControl.State) -> UIColor {
        
        switch controlState {
        case .normal:
            switch textStyle {
            case .largeInfo, .regularInfo, .smallInfo, .tinyInfo, .largeSuccess, .regularSuccess, .smallSuccess, .tinySuccess, .largeDanger, .regularDanger, .smallDanger, .tinyDanger, .largePrimary, .regularPrimary, .regularSecondary, .smallPrimary, .tinyPrimary,  .regularPrimaryBold, .largeWarning, .regularWarning, .smallWarning,  .tinyWarning, .largeLink,.regularLink, .tinyLink, .smallLink, .smallSecondary, .smallTertiary:
                return Color.transparent
            case .largePillInfo,  .regularPillInfo,  .smallPillInfo,  .tinyPillInfo:
                return Color.cyan
            case  .largePillSuccess,  .regularPillSuccess,  .smallPillSuccess,  .tinyPillSuccess:
                return Color.green
            case  .largePillDanger, .regularPillDanger,  .smallPillDanger,  .tinyPillDanger:
                return Color.red
            case  .largePillPrimary, .regularPillPrimary, .smallPillPrimary, .tinyPillPrimary:
                return Color.blue
            case .largePillWarning,  .regularPillWarning,  .smallPillWarning, .tinyPillWarning:
                return Color.orange
            }
        case .disabled:
            switch textStyle {
            case .largeInfo, .regularInfo, .smallInfo, .tinyInfo, .largeSuccess, .regularSuccess, .smallSuccess, .tinySuccess, .largeDanger, .regularDanger, .smallDanger, .tinyDanger, .largePrimary, .regularPrimary, .regularSecondary, .smallPrimary, .tinyPrimary,  .regularPrimaryBold, .largeWarning, .regularWarning, .smallWarning,  .tinyWarning, .largeLink,.regularLink, .tinyLink, .smallLink, .smallSecondary, .smallTertiary:
                return Color.transparent
            case .largePillInfo,  .regularPillInfo,  .smallPillInfo,  .tinyPillInfo:
                return Color.cyanDisabled
            case  .largePillSuccess,  .regularPillSuccess,  .smallPillSuccess,  .tinyPillSuccess:
                return Color.greenDisabled
            case  .largePillDanger, .regularPillDanger,  .smallPillDanger,  .tinyPillDanger:
                return Color.redDisabled
            case  .largePillPrimary, .regularPillPrimary, .smallPillPrimary, .tinyPillPrimary:
                return Color.blueDisabled
            case .largePillWarning,  .regularPillWarning,  .smallPillWarning, .tinyPillWarning:
                return Color.orange
            }
        case .highlighted:
            switch textStyle {
            case .largeInfo, .regularInfo, .smallInfo, .tinyInfo, .largeSuccess, .regularSuccess, .smallSuccess, .tinySuccess, .largeDanger, .regularDanger, .smallDanger, .tinyDanger, .largePrimary, .regularPrimary, .regularSecondary, .smallPrimary, .tinyPrimary,  .regularPrimaryBold, .largeWarning, .regularWarning, .smallWarning,  .tinyWarning, .largeLink,.regularLink, .tinyLink, .smallLink,  .smallSecondary, .smallTertiary:
                return Color.transparent
            case .largePillInfo,  .regularPillInfo,  .smallPillInfo,  .tinyPillInfo:
                return Color.cyanHighlighted
            case  .largePillSuccess,  .regularPillSuccess,  .smallPillSuccess,  .tinyPillSuccess:
                return Color.greenHighlighted
            case  .largePillDanger, .regularPillDanger,  .smallPillDanger,  .tinyPillDanger:
                return Color.redHighlighted
            case  .largePillPrimary, .regularPillPrimary, .smallPillPrimary, .tinyPillPrimary:
                return Color.blueHighlighted
            case .largePillWarning,  .regularPillWarning,  .smallPillWarning, .tinyPillWarning:
                return Color.orange
            }
        default:
            return .systemBackground
        }
    }
    
    public func textBgColor(_ textStyle: TextStyle) -> UIColor {
        switch textStyle {
        case .largeInfo, .regularInfo, .smallInfo, .tinyInfo, .largeSuccess, .regularSuccess, .smallSuccess, .tinySuccess, .largeDanger, .regularDanger, .smallDanger, .tinyDanger, .largePrimary, .regularPrimary, .regularSecondary, .smallPrimary, .tinyPrimary,  .regularPrimaryBold, .largeWarning, .regularWarning, .smallWarning,  .tinyWarning, .largeLink,.regularLink, .tinyLink, .smallLink, .smallSecondary, .smallTertiary:
            return Color.transparent
        case .largePillInfo,  .regularPillInfo,  .smallPillInfo,  .tinyPillInfo:
            return Color.cyan
        case  .largePillSuccess,  .regularPillSuccess,  .smallPillSuccess,  .tinyPillSuccess:
            return Color.green
        case  .largePillDanger, .regularPillDanger,  .smallPillDanger,  .tinyPillDanger:
            return Color.red
        case  .largePillPrimary, .regularPillPrimary, .smallPillPrimary, .tinyPillPrimary:
            return Color.primaryText
        case .largePillWarning,  .regularPillWarning,  .smallPillWarning, .tinyPillWarning:
            return Color.orange
        }
    }
    
    public var navigationBackgroundColor: UIColor {
        return .clear
    }

    public var navigationBarIsTranslucent: Bool {
        return true
    }

    public var CTAButtonColor: UIColor {
        return Color.blue
    }

    public var navigationBarFont: UIFont {
        return Font(.installed(.bold(.montserrat( fontNameProvider )) ), size: .standard(.headline)).instance
    }
    
    public func placeHolder(_ textStyle: TextStyle) -> UIColor {
        return Color.gray20
    }

    let fontNameProvider = MontserratFontProvider()
    
    public func font(_ textStyle: TextStyle) -> UIFont {
        switch textStyle {
        case .smallSecondary, .smallPillInfo, .smallPillSuccess, .smallPillDanger, .smallPillPrimary, .smallPillWarning, .smallInfo, .smallSuccess, .smallDanger, .smallPrimary, .smallWarning, .smallLink, .smallTertiary:
            return Font(.installed(.regular(.montserrat( fontNameProvider ))), size: .standard(.small)).instance
        case .tinyPillInfo, .tinyPillSuccess, .tinyPillDanger, .tinyPillPrimary, .tinyPillWarning, .tinyInfo, .tinySuccess, .tinyDanger, .tinyPrimary, .tinyWarning, .tinyLink:
            return Font(.installed(.regular(.montserrat( fontNameProvider ))), size: .standard(.tiny)).instance
        case .regularPillInfo, .regularPillSuccess, .regularPillDanger, .regularPillPrimary, .regularPillWarning, .regularInfo, .regularSuccess, .regularDanger, .regularPrimary, .regularSecondary, .regularWarning, .regularLink:
            return UIDevice().isSmallSize ? Font(.installed(.regular(.montserrat( fontNameProvider ))), size: .standard(.small)).instance : Font(.installed(.regular(.montserrat( fontNameProvider ))), size: .standard(.regular)).instance
        case .regularPrimaryBold:
            return Font(.installed(.bold(.montserrat( fontNameProvider ))), size: .standard(.regular)).instance
        case .largePillInfo, .largePillSuccess, .largePillDanger, .largePillPrimary, .largePillWarning, .largeInfo, .largeSuccess, .largeDanger, .largePrimary, .largeWarning, .largeLink:
            return UIDevice().isSmallSize ? Font(.installed(.bold(.montserrat( fontNameProvider ))), size: .standard(.regular)).instance : Font(.installed(.bold(.montserrat( fontNameProvider ))), size: .standard(.large)).instance
        }
    }
    
    public func textColor(_ textStyle: TextStyle) -> UIColor {
        switch textStyle {
        case .largePillPrimary, .largePillWarning, .regularPillWarning, .smallPillWarning, .tinyPillWarning, .largePillInfo, .regularPillInfo, .smallPillInfo, .tinyPillDanger, .regularPillPrimary, .smallPillPrimary, .tinyPillPrimary, .tinyPillInfo, .regularPillSuccess, .largePillSuccess, .smallPillSuccess, .tinyPillSuccess, .largePillDanger, .smallPillDanger, .regularPillDanger:
            return Color.white
        case .largeInfo, .regularInfo, .smallInfo, .tinyInfo:
            return Color.cyan
        case .largeSuccess, .regularSuccess, .smallSuccess, .tinySuccess:
            return Color.green
        case .largeDanger, .regularDanger, .smallDanger, .tinyDanger:
            return Color.red
        case .largePrimary, .regularPrimary, .smallPrimary, .tinyPrimary, .regularPrimaryBold:
            return Color.primaryText
        case .largeWarning, .regularWarning, .smallWarning, .tinyWarning:
            return Color.orange
        case .largeLink, .regularLink, .smallLink, .tinyLink:
            return Color.link
        case .smallSecondary, .regularSecondary:
            return Color.secondaryText
        case .smallTertiary:
            return Color.tertiaryText
        }
    }
    
    public var keyboardAppearance: UIKeyboardAppearance {
        return .light
    }
    
    public var viewBackgroundColor: UIColor {
        return .systemBackground
    }
    
    public var tabBarColor: UIColor {
        return Color.white
    }
    
    public var tintColor: UIColor {
        return .label
    }

    public var hLineColor: UIColor {
        return .separator
    }
}

