//
//  StackView.swift
//   2020
//

import UIKit

open class StackView: UIStackView {
    
    var themeStream: ThemeStreaming
    
    public init(themeStream: ThemeStreaming) {
        self.themeStream = themeStream
        super.init(frame: .zero)
        
        spacing = 1.dip
        //axis = .vertical
        //alignment = .fill
        //distribution = .equalCentering
        
        
        translatesAutoresizingMaskIntoConstraints = false
        axis = .vertical
        distribution = .fill
        alignment = .fill
    }
    
    required public init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
