//
//  ThemeStreaming.swift
//  Theme
//

import Foundation
import RxSwift

public protocol Themeable {
    var disposeBag: DisposeBag { get }
}

public protocol OptionalThemeable {
    var themeStream: ThemeStreaming? { get set }
    func applyTheme(theme: Theme?)
}

public protocol Theme: AnyObject {
    
    // View controller background color
    var type: ThemeType { get }
    var viewBackgroundColor: UIColor { get }
    var fadedBackgroundColor: UIColor { get }
    var tabBarColor: UIColor { get }
    var tintColor: UIColor { get }
    var navigationBarFont: UIFont { get }
    var navigationBackgroundColor: UIColor { get }
    var navigationBarIsTranslucent: Bool { get }

    // Font
    func font(_ textStyle: TextStyle) -> UIFont
    
    // Text color
    func textColor(_ textStyle: TextStyle) -> UIColor
    func placeHolder(_ textStyle: TextStyle) -> UIColor
    
    // Button background color
    func buttonBgColor(_ textStyle: TextStyle, for controlState: UIControl.State) -> UIColor
    var CTAButtonColor: UIColor { get }

    // Keyboard style
    var keyboardAppearance: UIKeyboardAppearance { get }
    
    // Horizontal Line Color
    var hLineColor: UIColor { get }
    
    // Button background color
    func textBgColor(_ textStyle: TextStyle) -> UIColor
    
    var optionSelectionColor: UIColor { get }
}

public protocol ThemeStreaming: AnyObject {
    var themeInUse: Observable<Theme> { get }
    
    func update(userInterfaceStyle: UIUserInterfaceStyle? )
}

public enum ThemeType: String {
    case dark
    case light
}

public class ThemeStreamFactory {
    
    static func produce(themeType: ThemeType) -> ThemeStreaming {
        return ThemeStream(themeType: themeType)
    }

}

public class ThemeStream: ThemeStreaming {
    
    public func update(userInterfaceStyle: UIUserInterfaceStyle?) {
        themeType = (userInterfaceStyle == .dark) ? .dark : .light
    }
    
    private var themeInUseSubject: BehaviorSubject<Theme>
    
    public var themeInUse: Observable<Theme> {
        return themeInUseSubject.asObservable()
    }
        
    private var themeType: ThemeType = .light {
        didSet {
            themeInUseSubject.onNext(ThemeStream.getTheme(themeType: themeType))
        }
    }

    public init(themeType: ThemeType) {
        self.themeInUseSubject = BehaviorSubject<Theme>(value: ThemeStream.getTheme(themeType: themeType))
    }
    
    private static func getTheme(themeType: ThemeType) -> Theme {
        switch themeType {
        case .dark:
            return DarkTheme()
        default:
            return LightTheme()
        }
    }
}

public final class AbsoluteTheme {
    public static var theme: Theme {
        return (UIScreen.main.traitCollection.userInterfaceStyle == .dark) ? DarkTheme() : LightTheme()
    }
}

open class TheameableViewController: UIViewController, Themeable {

    public var disposeBag: DisposeBag = DisposeBag()

    public var themeStream: ThemeStreaming

    public init(themeStream: ThemeStreaming) {
        self.themeStream = themeStream
        super.init(nibName: nil, bundle: nil)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func applyTheme(theme: Theme) {
        // No-OP
    }
}

extension UIColor {
    
    // MARK: - Initialization
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.currentIndex = scanner.string.startIndex
        var rgbValue: UInt64 = 0
        scanner.scanHexInt64(&rgbValue)

        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff

        self.init(red: CGFloat(r), green: CGFloat(g), blue: CGFloat(b), alpha: CGFloat(1))
    }
    
    // MARK: - Computed Properties
    
    var toHex: String? {
        return toHex()
    }
    
    // MARK: - From UIColor to String
    
    func toHex(alpha: Bool = false) -> String? {
        guard let components = cgColor.components, components.count >= 3 else {
            return nil
        }
        
        let r = Float(components[0])
        let g = Float(components[1])
        let b = Float(components[2])
        var a = Float(1.0)
        
        if components.count >= 4 {
            a = Float(components[3])
        }
        
        if alpha {
            return String(format: "%02lX%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255), lroundf(a * 255))
        } else {
            return String(format: "%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255))
        }
    }
    
}

