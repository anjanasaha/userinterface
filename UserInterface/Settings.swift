//
//  Settings.swift
//   2020
//

import UIKit
import RxSwift

public protocol BaseSettingsPresentableListener: class {
    func didTap(context: SettingsRowContext?)
}

public struct SettingsRowContext {
    var url: String? = nil
    var titleTextStyle: TextStyle = .largePrimary
    var form: Form? = nil
    var action: SettingsAction = .none
    var builder: FormItemBuildable? = nil
}

public enum SettingsAction {
    case logout, none
}

open class BaseSettingsViewController: ViewController {
    
    private let collectionView: CollectionView
    private var settingsDelegateDatasource: BaseSettingsDelegateDatasourceImpl
    private var viewIsVisible = false
    
    public var sections: [Section] {
        didSet {
            settingsDelegateDatasource.sections = sections
            collectionView.reloadData()
        }
    }
    
    public override init(themeStream: ThemeStreaming, keyboardObserver: KeyboardObserving? = nil ) {

        let layout = CollectionViewFlowLayout(scrollDirection: .vertical)
        layout.estimatedItemSize = CGSize(width: 100, height: 100)
        
        self.sections = []
        self.settingsDelegateDatasource = BaseSettingsDelegateDatasourceImpl(themeStream: themeStream, sections: [])
        self.collectionView = CollectionView(themeStream: themeStream, collectionViewLayout: layout)
        super.init(themeStream: themeStream, keyboardObserver: keyboardObserver)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func construct() {
        super.construct()

        collectionView.contentOffset = CGPoint(x: 0.dip, y: -2.5.dip)
        collectionView.showsVerticalScrollIndicator = false
        collectionView.dataSource = settingsDelegateDatasource
        collectionView.delegate = settingsDelegateDatasource
        collectionView.register(BasicCell.self, forCellWithReuseIdentifier: BaseSettingsDelegateDatasourceImpl.identifier)

        view.addSubview(collectionView)
    }
    
    open override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    open override func layout() {
        super.layout()
        collectionView.snp.makeConstraints {
            $0.trailing.bottom.equalTo(view.safeAreaLayoutGuide)
            $0.leading.equalTo(view.safeAreaLayoutGuide).offset(3.dip)
            $0.top.equalTo(horizontalLoading.snp.bottom)
        }
    }
}

open class BaseSettingsDelegateDatasourceImpl: NSObject, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    static let identifier = "HahaCell"
    private let themeStream: ThemeStreaming
    private var disposeBag: DisposeBag
    
    var sections: [Section]
    
    init(themeStream: ThemeStreaming, sections: [Section]) {
        self.sections = sections
        self.themeStream = themeStream
        self.disposeBag = DisposeBag()
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard sections.count > indexPath.section else { return UICollectionViewCell() }
        let section = sections[indexPath.section]
        
        guard section.rows.count > indexPath.row else { return  UICollectionViewCell() }
        let row = section.rows[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BaseSettingsDelegateDatasourceImpl.identifier, for: indexPath) as! BasicCell
        cell.titleLabel.text = row.title
        cell.subtitleLabel.text = row.subtitle
        
        apply(cell, cellThemingHandler: { (cell, currentTheme) in
            cell.applyTheme(theme: currentTheme)
            cell.titleLabel.font = currentTheme.font(.largePrimary)
            cell.titleLabel.textColor = currentTheme.textColor(row.context?.titleTextStyle ?? .largePrimary)
        })
        
        return cell
    }
    
    
    func apply(_ view: BasicCell, cellThemingHandler: @escaping BasicCellThemingHandler) {
        
        themeStream.themeInUse.subscribe(onNext: { (theme) in
            cellThemingHandler(view,theme)
        }).disposed(by: disposeBag)
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard sections.count > section else { return 0 }
        let aSection = sections[section]
        return aSection.rows.count
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sections.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard sections.count > indexPath.section else { return }
        let section = sections[indexPath.section]
        
        guard section.rows.count > indexPath.row else { return }
        let row = section.rows[indexPath.row]
        
        row.tapped()
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 4.dip)
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.25.dip
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.25.dip
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: 0, height: 3.dip)
    }
}
