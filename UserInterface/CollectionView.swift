//
//  CollectionView.swift
//   2020
//

import UIKit
import RxSwift

open class CollectionView: UICollectionView {

    private var themeStream: ThemeStreaming
    private var disposeBag = DisposeBag()

    public init(themeStream: ThemeStreaming, collectionViewLayout: CollectionViewFlowLayout) {
        self.themeStream = themeStream
        super.init(frame: .zero, collectionViewLayout: collectionViewLayout)
        
        themeStream.themeInUse.subscribe(onNext: { [weak self] (theme) in
            guard let self = self else { return }
            self.applyTheme(theme: theme)
        }).disposed(by: disposeBag)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func applyTheme(theme: Theme?) {
        guard let theme = theme else { return }
        backgroundColor = theme.viewBackgroundColor
    }
}

open class CollectionViewFlowLayout: UICollectionViewFlowLayout {

    public init(scrollDirection: UICollectionView.ScrollDirection) {
        super.init()
        self.scrollDirection = scrollDirection
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
