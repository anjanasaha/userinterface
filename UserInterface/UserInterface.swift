//
//  UserInterface.swift
//  UserInterface
//

import UIKit
import SnapKit
import RIBs
import RxSwift
import AVFoundation
import Lottie
import CommonLib

public protocol TabBarChildBuildable: Buildable {
    func build() -> TabBarChildRouting
}

public protocol TabBarChildRouting: ViewableRouting {
    var insideNavigationController: Bool { get }
}

public protocol TabBarControllable: ViewControllable {
    func add(childViewController: ViewControllable)
    func addChildViewControllerToNavStack(childViewController: ViewControllable)
}

public enum GutterConstants: CGFloat {
    case positiveDip = 16.0
    case negativeDip = -16.0
}

open class ViewController: UIViewController, Themeable, UIGestureRecognizerDelegate, KeyboardReactable {

    public final let gap = 2.dip
    public final let trailingGutter = GutterConstants.negativeDip.rawValue
    public final let leadingGutter = GutterConstants.positiveDip.rawValue
    public final let bottomGutter = GutterConstants.negativeDip.rawValue
    public final let topGutter = GutterConstants.positiveDip.rawValue

    final public var horizontalLoading: AnimationView

    weak private var _keyboardObserver: KeyboardObserving?
    weak private var _keyboardReactable: KeyboardReactable?
    public let themeStream: ThemeStreaming

    var loadingEnabled: Bool = false {
        didSet {
            if loadingEnabled {
                horizontalLoading.isHidden = false
                horizontalLoading.play(fromProgress: 0,
                                        toProgress: 1,
                                        loopMode: .autoReverse,
                                        completion: nil )
            } else {
                horizontalLoading.stop()
                horizontalLoading.isHidden = true
            }
        }
    }

    public var disposeBag: DisposeBag {
        return _internalDisposeBag ?? DisposeBag()
    }

    private var _internalDisposeBag: DisposeBag?
    private var pageTitleLabel: Label!
    private var backButton: Button!
    
    public init(themeStream: ThemeStreaming, keyboardObserver: KeyboardObserving? = nil) {
        self.themeStream = themeStream
        self._keyboardObserver = keyboardObserver
        self._internalDisposeBag = DisposeBag()
        let animation = Animation.named( "loading" , subdirectory: "") ?? nil
        self.horizontalLoading = AnimationView(animation: animation)
        self._keyboardObserver = keyboardObserver
        horizontalLoading.contentMode = .scaleAspectFill
        super.init(nibName: nil, bundle: nil)
        self._keyboardReactable = self
        configure()
    }
    
    open func configure() {
        construct()
        layout()
    }
    
    open func construct() {
        view.addSubview(horizontalLoading)
        loadingEnabled = false
        navigationItem.hidesBackButton = !isBackButtonEnabled()
    }
    
    open func isBackButtonEnabled() -> Bool {
        return true
    }
    
    open override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        _keyboardObserver?.status.subscribe(onNext: { [weak self] (tuple) in
            guard let self = self else { return }
            self.keyboardStatus(event: tuple.0, height: tuple.1)
        }).disposed(by: disposeBag)
    }
    
    open func keyboardStatus(event: KeyboardEvent, height: CGFloat) {
        view.needsUpdateConstraints()

        UIView.animate(withDuration: 0.35, animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

    open func layout() {
        horizontalLoading.snp.makeConstraints {
            $0.height.equalTo(0.25.dip)
            $0.leading.equalTo(view.snp.leading)
            $0.trailing.equalTo(view.safeAreaLayoutGuide)
            $0.top.equalTo(view.safeAreaLayoutGuide)
        }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    open override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        themeStream.update(userInterfaceStyle: previousTraitCollection?.userInterfaceStyle)
    }
    
    public func applyTheme(theme: Theme) {
        // No-OP
    }
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        themeStream.themeInUse.subscribe(onNext: { [weak self] (theme) in
            guard let self = self else { return }
            self.view.backgroundColor = theme.viewBackgroundColor
            
            if self != self.navigationController?.viewControllers.first {
                let backBTN = UIBarButtonItem(image: ImageLoader.load(source: .mainApp, name: "back"),
                                              style: .plain,
                                              target: self.navigationController,
                                              action: #selector(UINavigationController.popViewController(animated:)))
                self.navigationItem.leftBarButtonItem = backBTN
            }
            self.navigationController?.navigationBar.tintColor = theme.tintColor
            self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.font: theme.navigationBarFont, NSAttributedString.Key.foregroundColor: theme.tintColor]
            
            self.themeCustomizations.forEach { $0(theme) }
            
        }).disposed(by: _internalDisposeBag!)
    }
    
    deinit {
        _internalDisposeBag = nil
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        if (isMovingFromParent) {
            viewBeingDismissed(popped: true)
        }
    }
    
    open func viewBeingDismissed(popped: Bool) {
        // No-OP
    }
    
    final public func addThemeable(_ uiCustomization: @escaping (Theme)->()) {
        themeCustomizations.append(uiCustomization)
    }
    
    private var themeCustomizations = [(Theme)->()] ()
}

open class View: UIView { }

open class ThemedView: UIView, Themeable {
    
    public var disposeBag: DisposeBag {
        return _internalDisposeBag ?? DisposeBag()
    }

    private var _internalDisposeBag: DisposeBag?
    
    private var themeStream: ThemeStreaming
    
    final let trailingGutter = GutterConstants.negativeDip.rawValue
    final let leadingGutter = GutterConstants.positiveDip.rawValue
    final let bottomGutter = GutterConstants.negativeDip.rawValue
    final let topGutter = GutterConstants.positiveDip.rawValue
    
    init(frame: CGRect = .zero, themeStream: ThemeStreaming) {
        self.themeStream = themeStream
        super.init(frame: frame)
        
        themeStream.themeInUse.subscribe(onNext: { (theme) in
            self.backgroundColor = theme.viewBackgroundColor
        }).disposed(by: disposeBag)
        
        configure()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func construct() {}
    
    public func design() {}
    
    private func configure() {
        construct()
        design()
    }

    public func applyTheme(theme: Theme) {
        // NO-OP
    }
    
    fileprivate var activenessDisposable: CompositeDisposable? = CompositeDisposable()
    
    deinit {
        activenessDisposable?.dispose()
    }
}

public extension Disposable {

    @discardableResult
    func disposeOnDealloc(themedView: ThemedView) -> Disposable {
        if let activenessDisposable = themedView.activenessDisposable {
            _ = activenessDisposable.insert(self)
        } else {
            dispose()
        }
        return self
    }
}

open class TabBarController: UITabBarController, TabBarControllable, Themeable {
    
    public var themeStream: ThemeStreaming

    public var disposeBag: DisposeBag {
        return _internalDisposeBag ?? DisposeBag()
    }

    private var _internalDisposeBag: DisposeBag?
    
    init(themeStream: ThemeStreaming) {
        self.themeStream = themeStream
        super.init(nibName: nil, bundle: nil)
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        themeStream.themeInUse.subscribe(onNext: { [weak self] (theme) in
            self?.tabBar.tintColor = theme.tintColor
            self?.tabBar.barTintColor = theme.tabBarColor
        }).disposed(by: disposeBag)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func add(childViewController: ViewControllable) {
        addChild(childViewController.uiviewController)
    }

    public func addChildViewControllerToNavStack(childViewController: ViewControllable) {
        let navigationController = NavigationController(nibName: nil, bundle: nil)
        navigationController.pushViewController(childViewController.uiviewController, animated: true)

        if viewControllers == nil {
            viewControllers = [navigationController]
        } else {
            viewControllers?.append(navigationController)
        }
    }

    public func applyTheme(theme: Theme) {
        // No-OP
    }
}

open class Switch: UISwitch, Themeable {
    
    public var disposeBag: DisposeBag {
        return _internalDisposeBag ?? DisposeBag()
    }

    private var _internalDisposeBag: DisposeBag?
    
    public var themeStream: ThemeStreaming
    
    init(themeStream: ThemeStreaming) {
        self.themeStream = themeStream
        super.init(frame: .zero)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func applyTheme(theme: Theme) {
        // No-OP
    }
}

public protocol DeviceIndependentDisplaying {
    var dip: CGFloat { get }
}

extension Double: DeviceIndependentDisplaying {
    public var dip: CGFloat {
        return (UIDevice().isSmallSize ?  CGFloat(self) *  6.0 : CGFloat(self) *  8.0)
    }
}

//open class TextField: UITextField, Themeable {
//    
//    public var themeStream: ThemeStreaming
//    
//    private var currentTheme: Theme?
//    private var disposeBag = DisposeBag()
//    public var style: TextStyle {
//        didSet {
//            updateStyle()
//        }
//    }
//
//    open override var placeholder: String? {
//        didSet {
//            guard let theme = currentTheme else { return }
//            attributedPlaceholder = NSAttributedString(string: placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: theme.placeHolder(self.style) ])
//        }
//    }
//
//    public init(themeStream: ThemeStreaming, style: TextStyle) {
//        self.themeStream = themeStream
//        self.style = style
//        super.init(frame: .zero)
//        
//        themeStream.themeInUse.subscribe(onNext: { [weak self] (theme) in
//            guard let strongSelf = self else { return }
//            strongSelf.currentTheme = theme
//            strongSelf.updateStyle()
//        }).disposed(by: disposeBag)
//        
//        self.autocorrectionType = .no
//    }
//
//    required public init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//    
//    public func applyTheme(theme: Theme) {
//        updateStyle()
//    }
//
//    private func updateStyle() {
//        guard let theme = currentTheme else { return }
//        font = theme.font(style)
//        minimumFontSize = CGFloat((UIDevice().isSmallSize ? Font.FontSize.standard(.small).value : Font.FontSize.standard(.regular).value))
//        textColor = theme.color(style)
//        keyboardAppearance = theme.keyboardAppearance
//    }
//}

public extension UIView {
    func addGradientLayer() -> CAGradientLayer {
        
        let gradientLayer = CAGradientLayer()
        
        gradientLayer.frame = bounds
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.colors = [UIColor.red.cgColor, UIColor.systemGray2.cgColor, UIColor.red.cgColor]
        gradientLayer.locations = [0.0, 0.5, 1.0]
        layer.addSublayer(gradientLayer)
        
        return gradientLayer
    }
    
    func addAnimation() -> CABasicAnimation {
       
        let animation = CABasicAnimation(keyPath: "locations")
        animation.fromValue = [-1.0, -0.5, 0.0]
        animation.toValue = [1.0, 1.5, 2.0]
        animation.repeatCount = .infinity
        animation.duration = 0.9
        return animation
    }
    
    func startAnimating() {

        let gradientLayer = addGradientLayer()
        let animation = addAnimation()
       
        gradientLayer.add(animation, forKey: animation.keyPath)
    }
}

open class Label: UILabel, Themeable {

    public var themeStream: ThemeStreaming
    
    public var disposeBag: DisposeBag {
        return _internalDisposeBag ?? DisposeBag()
    }

    private var _internalDisposeBag: DisposeBag?
    
    public var style: TextStyle {
        didSet {
            restyle()
        }
    }
    
    private var currentTheme: Theme?

    public init(themeStream: ThemeStreaming, style: TextStyle) {
        self.themeStream = themeStream
        self.style = style
        
        super.init(frame: .zero)
        
        themeStream.themeInUse.subscribe(onNext: { [weak self] (theme) in
            self?.currentTheme = theme
            self?.applyTheme(theme: theme)
        }).disposed(by: disposeBag)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func applyTheme(theme: Theme) {
        font = theme.font(style)
        textColor = theme.textColor(style)
        backgroundColor = theme.textBgColor(style)
    }
    
    private func restyle() {
        font = currentTheme?.font(style)
        textColor = currentTheme?.textColor(style)
    }
}

open class HorizontalLine: UIView, OptionalThemeable {

    public var themeStream: ThemeStreaming?
    
    private var disposeBag = DisposeBag()
    
    public init(themeStream: ThemeStreaming) {
        self.themeStream = themeStream
        super.init(frame: .zero)
        themeStream.themeInUse.subscribe(onNext: { [weak self] (theme) in
            self?.applyTheme(theme: theme)
        }).disposed(by: disposeBag)
    }
    
    override public init(frame: CGRect) {
        let heightFrame = CGRect(x: frame.origin.x, y: frame.origin.y, width: frame.size.height, height: 0.125.dip)
        super.init(frame: heightFrame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func applyTheme(theme: Theme?) {
        guard let theme = theme else { return }
        backgroundColor = theme.hLineColor
    }
}

extension ImageButton: Pulseable {}

public class ImageButton: Button {
    
    public enum ImageButtonType: String {
        case queue
    }
    
    public init(themeStream: ThemeStreaming, style: TextStyle, eventID: EventID, type: ImageButtonType) {
        
        super.init(themeStream: themeStream, style: style, eventID: eventID)
        
        setImage(ImageLoader.load(source: .mainApp, name: type.rawValue).withTintColor(.white), for: .normal)
        contentMode = .scaleAspectFit
        imageEdgeInsets = UIEdgeInsets(top: 3, left: -2, bottom: 3, right: -2)
        
//        setImage(ImageLoader.load(source: .mainApp, name: type.rawValue).withTintColor(.white), for: .normal)
//        setImage(ImageLoader.load(source: .mainApp, name: type.rawValue).withTintColor(.white), for: .highlighted)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

public class LoadingButton: ThemedView {
    
    public let button: Button
    
    private var loading: AnimationView
    
    public var isEnabled: Bool = true {
        didSet {
            button.isEnabled = isEnabled
            if !isEnabled {
                stop()
            }
        }
    }
    
    public func play() {
        guard !loading.isAnimationPlaying else { return }
        loading.play(fromProgress: 0, toProgress: 1, loopMode: .loop, completion: nil)
        button.isHidden = true
        loading.isHidden = false
    }
    
    public func stop() {
        guard loading.isAnimationPlaying else { return }
        loading.stop()
        button.isHidden = false
        loading.isHidden = true
    }
    
    public init(themeStream: ThemeStreaming, style: TextStyle = .regularPrimary, eventID: EventID) {
        self.button = Button(themeStream: themeStream, style: style, eventID: eventID)
        self.loading = AnimationView(animation: AnimationLoader.load(source: .mainApp, name: "button_with_loading") )
        
        super.init(themeStream: themeStream)

        loading.contentMode = .scaleAspectFill
        
        button.addTarget(self, action: #selector(startPlaying), for: .touchUpInside)
        
        themeStream.themeInUse.subscribe(onNext: { [weak self] (theme) in
            guard let self = self else { return }
            self.backgroundColor = .clear
            self.button.applyTheme(theme: theme)
        }).disposeOnDealloc(themedView: self)
    }
    
    @objc private func startPlaying() {
        play()
    }
    
    public final override func construct() {
        addSubview(loading)
        addSubview(button)
        
        button.isHidden = false
        loading.isHidden = true
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        themify()
    }
    
    private func themify() {
        layer.cornerRadius = frame.height/2
        clipsToBounds = true
    }
    
    public final override func design() {
        button.snp.makeConstraints {  $0.trailing.bottom.leading.top.equalToSuperview() }
        loading.snp.makeConstraints {  $0.trailing.bottom.leading.top.equalToSuperview() }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

open class Button: UIButton, Themeable {
    
    public var themeStream: ThemeStreaming
    
    public var disposeBag: DisposeBag {
        return _internalDisposeBag ?? DisposeBag()
    }

    private var _internalDisposeBag: DisposeBag?
    
    private var style: TextStyle
    
    private let eventID: EventID
    
    private var theme: Theme?

    public var height: CGFloat {
        switch style {
        case .largeSuccess, .largeInfo, .largeWarning, .largeDanger, .largePrimary, .largeLink, .largePillInfo, .largePillSuccess, .largePillDanger, .largePillPrimary, .largePillWarning:
            return Height.large
        case .smallSecondary, .smallInfo, .smallWarning, .smallDanger, .smallPrimary, .smallSuccess, .smallPillDanger, .smallPillSuccess, .smallPillPrimary, .smallLink, .smallPillInfo, .smallPillWarning, .smallTertiary:
            return Height.small
        case .regularSuccess, .regularInfo, .regularWarning, .regularDanger, .regularPrimary, .regularPrimaryBold, .regularLink, .regularPillInfo, .regularPillSuccess, .regularPillDanger, .regularPillPrimary, .regularPillWarning, .regularSecondary:
            return Height.regular
        case .tinyPillSuccess, .tinyInfo, .tinySuccess, .tinyDanger, .tinyPrimary, .tinyWarning, .tinyLink, .tinyPillInfo, .tinyPillDanger, .tinyPillPrimary, .tinyPillWarning:
            return Height.tiny
        }
    }
    
    private struct Height {
        static let tiny = 3.5.dip
        static let small = 4.0.dip
        static let regular = 5.dip
        static let large = 6.5.dip
    }

    open override var isHighlighted: Bool {
        didSet {
            guard let theme = theme else { return }
            applyTheme(theme: theme)
        }
    }

    open override var isEnabled: Bool{
        didSet {
            guard let theme = theme else { return }
            applyTheme(theme: theme)
        }
    }
    
    public init(themeStream: ThemeStreaming, style: TextStyle, eventID: EventID) {
        self.themeStream = themeStream
        self.style = style
        self.eventID = eventID

        super.init(frame: .zero)
        
        themeStream.themeInUse.subscribe(onNext: { [weak self] (theme) in
            guard let strongSelf = self else { return }
            strongSelf.applyTheme(theme: theme)
        }).disposed(by: disposeBag)
        
        addTarget(self, action: #selector(sendAnalytics), for: .touchUpInside)
    }
    
    @objc func sendAnalytics() {
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setTextStyle(_ textStyle: TextStyle, for state: UIControl.State) {
        setTitleColor(theme?.textColor(textStyle) , for: state)
    }
    
    private func themeApply() {
        
        guard  let theme = theme else { return }
        
        // Title color
        setTitleColor(theme.textColor(style), for: .normal)
        setTitleColor(Color.gray40 , for: .disabled)

        titleLabel?.font = theme.font(self.style)
        tintColor = theme.textColor(self.style)
        backgroundColor = theme.buttonBgColor(self.style, for: self.state)
        
        switch style {
        case .smallPillInfo, .smallPillSuccess, .smallPillDanger, .smallPillPrimary, .smallPillWarning, .tinyPillInfo, .tinyPillSuccess, .tinyPillDanger, .tinyPillPrimary, .tinyPillWarning, .regularPillInfo, .regularPillSuccess, .regularPillDanger, .regularPillPrimary, .regularPillWarning, .largePillInfo, .largePillSuccess, .largePillDanger, .largePillPrimary,.largePillWarning:
            contentEdgeInsets = UIEdgeInsets(top: 1.dip, left: 1.5.dip, bottom: 1.dip, right: 1.5.dip)
            layer.cornerRadius = frame.height / 2
        default:
            contentEdgeInsets = UIEdgeInsets(top: 0, left: 0.dip, bottom: 0, right: 0.dip)
        }
        
        layer.masksToBounds = true
        clipsToBounds = true
    }

    open override func layoutSubviews() {
        super.layoutSubviews()
        themeApply()
    }
    
    public func applyTheme(theme: Theme) {
        self.theme = theme
        themeApply()
    }
    
    public func clearAllTargets() {
        removeTarget(nil, action: nil, for: .allEvents)
    }
}

open class TapGestureRecognizer: UITapGestureRecognizer {
    
}

open class ImageView: UIImageView {

    var themeStream: ThemeStreaming
    var shadow: Bool = false

    public init(frame:CGRect = .zero, themeStream: ThemeStreaming, shadow: Bool = false) {
        self.themeStream = themeStream
        super.init(frame: .zero)
        
        configure()
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure() {
        construct()
        layout()
    }
    
    func construct() { }
    
    override open func layoutSubviews() {
        super.layoutSubviews()

        if shadow {
            let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: frame.size.height / 2)
            layer.shadowColor = Color.gray20.cgColor
            layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
            layer.shadowOpacity = 0.5
            layer.shadowPath = shadowPath.cgPath
            layer.masksToBounds = false
            layer.borderWidth = 0.25.dip
            layer.borderColor = Color.gray40.cgColor
            layer.cornerRadius = frame.size.height / 2
        }
    }
    
    func layout() { }
}

public protocol ListDisplayable {
    var title: String { get }
    var subtitle: String { get }
    var icon: UIImage? { get }
    var iconUrl: String { get }
}

open class GenericListCell: UICollectionViewCell {
    
    var titleLabel: UILabel!
    var subtitleLabel: UILabel!
    var rule: HorizontalLine!
    var showHorizontalLine: Bool = true {
        didSet {
            rule.isHidden = !showHorizontalLine
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        titleLabel = UILabel(frame: .zero)
        subtitleLabel = UILabel(frame: .zero)
        rule = HorizontalLine(frame: .zero)

        addSubview(titleLabel)
        addSubview(subtitleLabel)
        addSubview(rule)

        setupConstraints(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func applyTheme(theme: Theme?) {
        guard let theme = theme else { return }
        titleLabel.font = theme.font(.regularPrimaryBold)
        titleLabel.textColor = theme.textColor(.regularPrimaryBold)
        
        subtitleLabel.font = theme.font(.smallSecondary)
        subtitleLabel.textColor = theme.textColor(.smallSecondary)
        
        rule.backgroundColor = theme.hLineColor
        backgroundColor = theme.viewBackgroundColor
    }
    
    private func setupConstraints(frame: CGRect) {
        snp.makeConstraints { (maker) in
            maker.height.equalTo(10.dip)
            maker.width.equalTo(frame.width)
        }
        titleLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(self).offset(1.5.dip)
            maker.leading.equalTo(self).offset(2.dip)
            maker.trailing.equalTo(self).offset(-3.dip)
            maker.height.equalTo(4.dip)
        }
        subtitleLabel.snp.makeConstraints { (maker) in
            maker.leading.equalTo(self).offset(2.dip)
            maker.trailing.equalTo(self).offset(-3.dip)
            maker.top.equalTo(titleLabel.snp.bottom)
            maker.height.equalTo(3.dip)
        }
        rule.snp.makeConstraints { (maker) in
            maker.bottom.trailing.equalToSuperview()
            maker.leading.equalToSuperview().offset(2.dip)
            maker.height.equalTo(1)
        }
    }
}
open class Slider: UISlider {

}

public typealias BasicCellThemingHandler = (_ view: BasicCell,_ theme: Theme) -> Void
public typealias GenericCellThemingHandler = (_ view: GenericListCell,_ theme: Theme) -> Void

struct CellConfigurator<Model> {
    
    var titleKeyPath: WritableKeyPath<Model, String>
    var subtitleKeyPath: WritableKeyPath<Model, String>

    func configure(_ cell: UITableViewCell, for model: Model) {
        cell.textLabel?.text = model[keyPath: titleKeyPath]
        cell.detailTextLabel?.text = model[keyPath: subtitleKeyPath]
    }
}

public class Section {
    public let rows: [Row]
    
    public init(rows: [Row]) {
        self.rows = rows
    }
}

open class Row: NSObject {
    public let title: String
    public let subtitle: String
    public let context: SettingsRowContext?
    public var tapHandler: ((SettingsRowContext?) -> Void)?
    public var isSelected: Bool
    
    public init(title: String, subtitle: String = "", completion: ((Row, SettingsRowContext) -> Void)? = nil, builder: FormItemBuildable? = nil, context: SettingsRowContext? = nil, isSelected: Bool = false) {
        self.title = title
        self.subtitle = subtitle
        self.tapHandler = nil
        self.context = context
        self.isSelected = isSelected
        super.init()
    }
    
    func tapped() {
        tapHandler?(context)
    }

    override public var debugDescription: String {
        return "\(title) - \(subtitle)"
    }
}

extension Row: ListDisplayable {
    public var icon: UIImage? {
        return nil
    }
    
    public var iconUrl: String {
        return ""
    }
}

public class TitleSubtitleView: ThemedView {
    let titleLabel: Label
    let subtitleLabel: Label

    private let tap: TapGestureRecognizer
    private let title: String
    private let subtitle: String
    
    init(frame: CGRect, themeStream: ThemeStreaming, title: String, subtitle: String) {
        self.titleLabel = Label(themeStream: themeStream, style: .regularPrimaryBold)
        self.subtitleLabel = Label(themeStream: themeStream, style: .smallSecondary)
        self.tap = TapGestureRecognizer()
        self.title = title
        self.subtitle = subtitle
        super.init(themeStream: themeStream)
    
        configure()
    }
    
    public func addTarget(target: Any, action: Selector) {
        tap.addTarget(target, action: action)
    }

    private func configure() {
        build()
        layout()
    }
    
    private func build() {
        
        addGestureRecognizer(tap)

        titleLabel.text = title
        subtitleLabel.text = subtitle
        
        addSubview(titleLabel)
        
        if !subtitle.isEmpty {
             addSubview(subtitleLabel)
        }
       
    }
    
    private func layout() {
        
        if subtitleLabel.superview != nil {
            titleLabel.snp.makeConstraints {
                $0.leading.equalTo(safeAreaLayoutGuide).offset(2.dip)
                $0.top.equalTo(safeAreaLayoutGuide).offset(1.dip)
                $0.trailing.equalTo(safeAreaLayoutGuide).offset(-2.dip)
            }
            
            subtitleLabel.snp.makeConstraints {
                $0.leading.equalTo(safeAreaLayoutGuide).offset(2.dip)
                $0.top.equalTo(titleLabel.snp.bottom).offset(1.dip)
                $0.trailing.equalTo(safeAreaLayoutGuide).offset(-2.dip)
                $0.bottom.equalTo(safeAreaLayoutGuide).offset(-1.dip)
            }
        } else {
            titleLabel.snp.makeConstraints {
                $0.leading.equalTo(safeAreaLayoutGuide).offset(2.dip)
                $0.top.equalTo(safeAreaLayoutGuide).offset(1.dip)
                $0.trailing.equalTo(safeAreaLayoutGuide).offset(-2.dip)
                $0.bottom.equalTo(safeAreaLayoutGuide).offset(-1.dip)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

public extension UIViewController {

    func presentInFullScreen(_ viewController: UIViewController,
                           animated: Bool,
                           completion: (() -> Void)? = nil) {
        viewController.modalPresentationStyle = .fullScreen
        present(viewController, animated: animated, completion: completion)
    }
}

final public class CaptureButton: Button {
    
    override public init(themeStream: ThemeStreaming, style: TextStyle, eventID: EventID) {
        super.init(themeStream: themeStream, style: style, eventID: .assignEventID)
        backgroundColor = .white
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

final public class InteractiveImageView: ImageView {

    private var deleteButton: Button
    var deleteBlock: (()->())? {
        didSet {
            deleteButton.isHidden = (self.deleteBlock == nil)
        }
    }

    public init(frame: CGRect, themeStream: ThemeStreaming ) {
        self.deleteButton = Button(themeStream: themeStream, style: .smallDanger , eventID: .assignEventID)
        super.init(frame: frame, themeStream: themeStream)
        
        deleteButton.addTarget(self, action: #selector(deleteTapped(_:)), for: .touchUpInside)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func construct() {
        addSubview(deleteButton)
        
        if #available(iOS 13.0, *) {
            deleteButton.setImage(UIImage(named: "trash")?.withTintColor(.white) , for: .normal)
        } else {
            deleteButton.setImage(UIImage(named: "trash")?.withRenderingMode(.alwaysTemplate) , for: .normal)
            deleteButton.tintColor = .white
        }
    }
    
    override func layout() {
        deleteButton.snp.makeConstraints {
            $0.height.width.equalTo(3.dip)
            $0.leading.equalToSuperview().offset(1.dip)
            $0.bottom.equalToSuperview().inset(1.dip)
        }
    }
    
    @objc func deleteTapped(_ sender: Button) {
        deleteBlock?()
    }
}


public class BasicCell: UICollectionViewCell {

    var titleLabel: UILabel!
    var subtitleLabel: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.translatesAutoresizingMaskIntoConstraints = false

        titleLabel = UILabel(frame: .zero)
        subtitleLabel = UILabel(frame: .zero)

        addSubview(titleLabel)
        addSubview(subtitleLabel)

        setupConstraints(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func applyTheme(theme: Theme?) {
        guard let theme = theme else { return }
        titleLabel.font = theme.font(.regularPrimaryBold)
        titleLabel.textColor = theme.textColor(.regularPrimaryBold)

        subtitleLabel.font = theme.font(.regularPrimary)
        subtitleLabel.textColor = theme.textColor(.regularPrimary)

        backgroundColor = theme.viewBackgroundColor
    }

    private func setupConstraints(frame: CGRect) {
        let titleLabelHeight = 4.dip
        let subtitleLabelHeight = 3.dip
        let total = titleLabelHeight + subtitleLabelHeight

        snp.makeConstraints { (maker) in
            maker.height.equalTo(total)
            maker.width.equalTo(frame.width)
        }
        titleLabel.snp.makeConstraints { (maker) in
            maker.top.trailing.equalTo(self)
            maker.leading.equalToSuperview()
            maker.height.equalTo(titleLabelHeight)
        }
        subtitleLabel.snp.makeConstraints { (maker) in
            maker.trailing.equalTo(self)
            maker.leading.equalToSuperview()
            maker.top.equalTo(titleLabel.snp.bottom)
            maker.height.equalTo(subtitleLabelHeight)
        }
    }
}

public class FontLoader {
    
    static func loadAll(_ fontFileNames: [String]) {
        DispatchQueue.main.async {
            do {
                try loadFonts(fontFileNames).forEach({ try register(from: $0) })
            } catch {
                print(error)
            }
        }
    }
    
    static func register(from url: URL) throws {
        guard let fontDataProvider = CGDataProvider(url: url as CFURL) else { return }
        guard let font = CGFont(fontDataProvider) else { return }
        var error: Unmanaged<CFError>?
            
        guard CTFontManagerRegisterGraphicsFont(font, &error) else { return }
    }
    
    static var resourceBundle: Bundle {
        return Bundle.main
    }
    
    static func loadFonts(_ fontFileNames: [String]) -> [URL] {
        return fontFileNames.map({ resourceBundle.url(forResource: $0, withExtension: "ttf")! })
    }
}

public class AnimationLoader {
    
    public static func load(source: BundleSourceType, name: String) -> Animation? {
        let uistyle: ThemeType = UIScreen.main.traitCollection.userInterfaceStyle == .dark ? .dark : .light
        switch source {
        case .other(let identifier):
            guard let bundlePath = Bundle.main.path(forResource: identifier, ofType: "bundle"), let bundle = Bundle.init(path: bundlePath) else { return nil }
            guard let animation =  Animation.named("\(name)_\(uistyle.rawValue)", bundle: bundle, subdirectory: nil, animationCache: nil) else { return nil }
            return animation
        default:
            guard let animation =  Animation.named("\(name)_\(uistyle.rawValue)", bundle: Bundle.main, subdirectory: nil, animationCache: nil) else { return nil }
            return animation
        }
    }
}

public enum EventID {
    case val(String)
    case assignEventID
}

open class GridBox {
    
    public let title: String
    
    public let subtitle: String
    
    public let iconUrl: String?
    
    public init(title: String, subtitle: String, iconUrl: String?) {
        self.title = title
        self.subtitle = subtitle
        self.iconUrl = iconUrl
    }
}

public class GridSection {
    
    public let boxes: [GridBox]
    
    public init(boxes: [GridBox]) {
        self.boxes = boxes
    }
}
