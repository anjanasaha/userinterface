//
//  Pulseable.swift
//  2020
//

import UIKit

private var pulseLayerKey: Void?
private let externalLayerName = "YGLayerName"
private let pulseKey = "PulseKey"
private let radarKey = "RadarKey"

public protocol Pulseable {
     func addPulse(startScale: CGFloat, finishScale: CGFloat, frequency: CFTimeInterval, opacity: Float) -> Pulseable
     func resumePulse()
     func suspendPulse()
     func cancelPause()
}

extension Pulseable where Self: Button {

    var animationLayer: CALayer {
        get {
            if let object = objc_getAssociatedObject(self, &pulseLayerKey) as? CALayer {
                return object
            }
            let externalBorder = CALayer()
            externalBorder.frame = frame
            externalBorder.cornerRadius = layer.cornerRadius
            externalBorder.name = externalLayerName
            objc_setAssociatedObject(self, &pulseLayerKey, externalBorder, .OBJC_ASSOCIATION_RETAIN)
            return externalBorder
        }
        set {
           objc_setAssociatedObject(self, &pulseLayerKey, newValue, .OBJC_ASSOCIATION_RETAIN)
        }
    }

    @discardableResult
    public func addPulse(startScale: CGFloat = 1.0, finishScale: CGFloat = 1.4, frequency: CFTimeInterval = 1.0, opacity: Float = 0.7) -> Pulseable {
        animationLayer.backgroundColor = backgroundColor?.cgColor
        animationLayer.opacity = opacity
        layer.masksToBounds = false
        layer.superlayer?.insertSublayer(animationLayer, below: layer)
        
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.fromValue = startScale
        scaleAnimation.toValue = finishScale
        scaleAnimation.autoreverses = false
        scaleAnimation.duration = frequency
        scaleAnimation.repeatCount = Float.greatestFiniteMagnitude
        animationLayer.add(scaleAnimation, forKey: pulseKey)
       
        let opacityAnimation = CABasicAnimation(keyPath: "opacity")
        opacityAnimation.fromValue = opacity
        opacityAnimation.toValue = 0.0
        opacityAnimation.autoreverses = false
        opacityAnimation.duration = frequency
        opacityAnimation.repeatCount = Float.greatestFiniteMagnitude
        animationLayer.add(opacityAnimation, forKey: radarKey)
        
        animationLayer.speed = 0.0
        return self
    }
   /// Resume/start animation
    public func resumePulse() {
        let pausedTime = animationLayer.timeOffset
        animationLayer.speed = 1.0
        animationLayer.timeOffset = 0.0
        animationLayer.beginTime = 0.0
        let timeSincePauce = animationLayer.convertTime(CACurrentMediaTime(), from: nil) - pausedTime
        animationLayer.beginTime = timeSincePauce
    }
         /// Pause animation
    public func suspendPulse() {
       let pausedTime = animationLayer.convertTime(CACurrentMediaTime(), from: nil)
        animationLayer.speed = 0.0
        animationLayer.timeOffset = pausedTime
    }
         /// Remove the animation
    public func cancelPause() {
        layer.removeAnimation(forKey: pulseKey)
        layer.removeAnimation(forKey: radarKey)
        animationLayer.removeFromSuperlayer()
    }
}

