//
//  Font.swift
//  Hopshop
//

import UIKit

public enum TextStyle {
    
    // large
    case largeInfo
    case largeSuccess
    case largeDanger
    case largePrimary
    case largeWarning
    case largeLink
    
    // Pills large
    case largePillInfo
    case largePillSuccess
    case largePillDanger
    case largePillPrimary
    case largePillWarning
    
    // regular
    case regularInfo
    case regularSuccess
    case regularDanger
    case regularPrimary
    case regularSecondary
    case regularPrimaryBold
    case regularWarning
    case regularLink
    
    // Pills regular
    case regularPillInfo
    case regularPillSuccess
    case regularPillDanger
    case regularPillPrimary
    case regularPillWarning

    // Tiny
    case tinyInfo
    case tinySuccess
    case tinyDanger
    case tinyPrimary
    case tinyWarning
    case tinyLink
    
    // Pills tiny
    case tinyPillInfo
    case tinyPillSuccess
    case tinyPillDanger
    case tinyPillPrimary
    case tinyPillWarning

    // small
    case smallInfo
    case smallSuccess
    case smallDanger
    case smallPrimary
    case smallSecondary
    case smallTertiary
    case smallWarning
    case smallLink
    
    // Pills small
    case smallPillInfo
    case smallPillSuccess
    case smallPillDanger
    case smallPillPrimary
    case smallPillWarning
}

public enum FontFamily {
    case montserrat(FontNameProviding)
}

public protocol FontNameProviding {
    func name(_ fw: Font.FontWeight) -> String
}

public typealias FontWeight = Font.FontWeight

public struct Font {
    
    enum FontType {
        case installed(FontWeight)
    }
    
    public enum FontSize {
        case standard(StandardSize)
        case custom(Double)
        public var value: Double {
            switch self {
            case .standard(let size):
                return size.rawValue
            case .custom(let customSize):
                return customSize
            }
        }
    }
    
    public enum FontWeight {
        case bold(FontFamily)
        case extraBold(FontFamily)
        case extraLight(FontFamily)
        case light(FontFamily)
        case medium(FontFamily)
        case regular(FontFamily)
        case semiBold(FontFamily)
        case thin(FontFamily)
    }
    
    public enum StandardSize: Double {
        case logo = 48.0
        case headline = 36.0
        case large = 24.0
        case regular = 18.0
        case small = 14.0
        case tiny = 10.0
    }
    
    
    var type: FontType
    var size: FontSize
    init(_ type: FontType, size: FontSize) {
        self.type = type
        self.size = size
    }
}

public extension Font {
    
    var instance: UIFont {
        
        var instanceFont: UIFont!
        var fontName = ""
        
        switch type {
        case .installed(let weight):
            switch weight {
            case .bold(let family):
                switch family {
                case .montserrat(let provider):
                    fontName = provider.name(weight)
                }
            case .extraBold(let family):
                switch family {
                case .montserrat(let provider):
                    fontName = provider.name(weight)
                }
            case .extraLight(let family):
                switch family {
                case .montserrat(let provider):
                    fontName = provider.name(weight)
                }
            case .light(let family):
                switch family {
                case .montserrat(let provider):
                    fontName = provider.name(weight)
                }
            case .medium(let family):
                switch family {
                case .montserrat(let provider):
                    fontName = provider.name(weight)
                }
            case .regular(let family):
                switch family {
                case .montserrat(let provider):
                    fontName = provider.name(weight)
                }
            case .semiBold(let family):
                switch family {
                case .montserrat(let provider):
                    fontName = provider.name(weight)
                }
            case .thin(let family):
                switch family {
                case .montserrat(let provider):
                    fontName = provider.name(weight)
                }
            }
            guard let font =  UIFont(name: fontName , size: CGFloat(size.value)) else {
                return UIFont.systemFont(ofSize: 14.0, weight: .heavy)
            }
            instanceFont = font
        }
        return instanceFont
    }
}

class Utility {
    /// Logs all available fonts from iOS SDK and installed custom font
    class func logAllAvailableFonts() {
        for family in UIFont.familyNames {
            for name in UIFont.fontNames(forFamilyName: family) {
                print("   \(name)")
            }
        }
    }
}
